package com.drawm.mvc {
	
	import com.drawm.mvc.data.DisplayData;
	
	import flash.display.Stage;
	import flash.display.StageOrientation;
import flash.geom.Rectangle;
	import flash.system.Capabilities;
	

	import starling.core.Starling;

	public class Starter extends Starling{
		
		private static var instance : Starter;

        private var _displayData : DisplayData;


		public function Starter(rootClass:Class, stage:Stage){
			instance = this;
            _displayData = DisplayData.instance;

			validateDisplay(stage, stage.orientation);
			
			super(rootClass, stage, new Rectangle(0,0,_displayData.width,_displayData.height));
		}
		
		public static function addEventListener(type:String, listener:Function):void{
			instance.addEventListener(type, listener);
		}
		public static function removeEventListener(type:String, listener:Function):void{
			instance.removeEventListener(type, listener);
		}
		public static function removeEventListeners(type:String=null):void{
			instance.removeEventListeners(type);
		}
		
		protected function validateDisplay(stage:Stage, orientation:String):void {
			if (_displayData.platform == DisplayData.UNDEFINED) {
				if(Capabilities.version.substr(0,3) == "AND" || Capabilities.version.substr(0,3) == "IOS"){
					// Défini la taille de l'application
					
					//*  // Testing on air mobile
                    _displayData.width  = stage.fullScreenWidth;
                    _displayData.height = stage.fullScreenHeight;
					/*/ // Release on mobile
					display.appWidth  	= Capabilities.screenResolutionX;
					display.appHeight 	= Capabilities.screenResolutionY;
					//*/
					
					// Défini le type de plateforme
					if (Capabilities.screenDPI > 400)
                        _displayData.platform = DisplayData.PLATFORM_TABLET;
					else
                        _displayData.platform = DisplayData.PLATFORM_MOBILE;
					
					if (_displayData.width > _displayData.height)
                        _displayData.orientation = DisplayData.RACIO_LANDSCAPE;
					else
                        _displayData.orientation = DisplayData.RACIO_PORTRAIT;
					
					if (_displayData.defaultOrientation == DisplayData.UNDEFINED)
                        _displayData.defaultOrientation = _displayData.orientation;
					
				}else if (Capabilities.version.substr(0, 3) == "WIN" || Capabilities.version.substr(0, 3) == "MAC" || Capabilities.version.substr(0, 3) == "LNX"){
					// Défini la taille de l'application
                    _displayData.width  = stage.stageWidth;
                    _displayData.height = stage.stageHeight;
					// Défini le type de plateforme
                    _displayData.platform = DisplayData.PLATFORM_DESKTOP;
				}
			}else if(_displayData.platform == DisplayData.PLATFORM_DESKTOP){
                _displayData.width  = stage.stageWidth;
                _displayData.height = stage.stageHeight;
				
			}else { // On mobile
				/*  // Testing on air mobile
				display.appWidth  = stage.stageWidth;
				display.appHeight = stage.stageHeight;
				/*/ // Release on mobile
                _displayData.width  	= Capabilities.screenResolutionX;
                _displayData.height 	= Capabilities.screenResolutionY;
				//*/
				if (orientation == StageOrientation.DEFAULT || 
					orientation == StageOrientation.UPSIDE_DOWN) {

                    _displayData.orientation = _displayData.defaultOrientation ;
					
				}else if (orientation == StageOrientation.ROTATED_LEFT || 
					orientation == StageOrientation.ROTATED_RIGHT) {
					
					if (_displayData.defaultOrientation == DisplayData.RACIO_PORTRAIT)
                        _displayData.orientation = DisplayData.RACIO_LANDSCAPE ;
					else
                        _displayData.orientation = DisplayData.RACIO_PORTRAIT ;
				}
			}
		}
	}
}