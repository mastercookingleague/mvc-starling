package com.drawm.mvc.data{
	import feathers.system.DeviceCapabilities;

import flash.display.Stage;

import starling.core.Starling;

	public class DisplayData{
		
		public static const UNDEFINED 	     : uint = 0;
		
		public static const PLATFORM_MOBILE  : uint = 1;
		public static const PLATFORM_TABLET  : uint = 2;
		public static const PLATFORM_DESKTOP : uint = 3;
		
		public static const RACIO_PORTRAIT 	 : uint = 4;
		public static const RACIO_LANDSCAPE	 : uint = 5;

		
		public var width    : uint = 0;
		public var height   : uint = 0;
		public var platform    : uint = UNDEFINED;
		public var orientation : uint = UNDEFINED;
		public var defaultOrientation : uint = UNDEFINED;

		public function get isPortrait():Boolean {
			return !isLandscape;
		}
		public function get isLandscape():Boolean {
			return (width > height);
		}

        private var _isPhone:Boolean;
		public function get isPhone():Boolean {
            if(_isPhone == null)
                _isPhone = DeviceCapabilities.isPhone(Starling.current.nativeStage);
			return _isPhone;
		}
		
		// SINGLETON
		private static var _instance:DisplayData;
		public function DisplayData(){
			if(_instance) {
				throw(new Error("DisplayData is a singleton"),9001);
            } else {
				_instance = this;
            }
		}
		public static function get instance():DisplayData{
			if(!_instance)
				new DisplayData();
			return _instance;
		}
		public function toString():String{
			return JSON.stringify(this);
		}
	}
}