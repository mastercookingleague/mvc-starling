package com.drawm.mvc{

import com.drawm.mvc.data.DisplayData;

import flash.desktop.NativeApplication;
import flash.events.NativeWindowBoundsEvent;

import flash.geom.Rectangle;

import starling.display.Sprite;
	import com.drawm.mvc.controler.Controler;
	import com.drawm.mvc.event.MVCEvent;
	import com.drawm.mvc.model.Model;
	import com.drawm.mvc.view.View;
	
	import flash.events.StageOrientationEvent;
	import flash.system.System;

	import starling.core.Starling;
	import starling.display.DisplayObjectContainer;
	import starling.events.Event;
	import starling.events.ResizeEvent;

	public class Page extends Sprite {
		
		private var currentOrientation : String = "";

        private var _displayData : DisplayData;
		
		/**
		 * Défini si la fonction start a déjà été appelé
		 * Si isStarted == true, l'application contient ses élément graphiques 
		 * et les informations nécéssaires pour qu'elle sois fonctionnel.
		 * Il est donc inutile d'appeler start dans ce cas, il faut plutot appeler resume();
		 */
		private var _isStarted:Boolean = false;
		
		protected var _model : Model;
		protected var _view	 : View;

		public function get _controler():Controler {
			return __controler;
		}

		public function set _controler(value:Controler):void {	
			statusStack.push("_controler from "+__controler+" to "+value);
			__controler = value;
		}

		public function get isStarted():Boolean {
			return _isStarted;
		}
		private var _isPaused:Boolean = false;
		public function get isPaused():Boolean {
			return _isPaused;
		}
		
		public function get view():View{
			return _view;
		}
		protected var __controler : Controler;
		
		/**
		 * Défini la quantité le frame que la page doit attendre avant de pouvoir utiliser flatten();
		 */
		private var _nbFrameToFullInit:int = 3;

		/**
		 * Défini si la page est visible / est complètement initialisé
		 */
		private var _isInitialized:Boolean = false;
		
		/**
		 * Garde men mémoire la dernière trnasition utilisé sur la page
		 */
		public var lastTransition:uint;
		
		public var statusStack:Array = new Array();
		
		public function Page(model:Class,view:Class,controler:Class){
			this.addEventListener(Event.ADDED_TO_STAGE, screen_addedToStageHandler);

            // Init les variables nécéssaires au fonctionnement de la page
            _displayData = DisplayData.instance;
            _isInitialized = false;

            // Init le model
			_model = Model(new model());
			_model.currentPage = this;

            // Init la view de base
			_view = View(new view(_model));

            // Init le controler
			_controler = Controler(new controler(_model,this));

			super();
		}
		
		private function restartPage():void {
			if(isStarted){
				if(!isPaused){
					pause();
				
					_isInitialized = false;
					resume();
				}
			}
		}
		
		// LEGACY' LEGACY EVERYWHERE
		protected function removeAllChild(target:DisplayObjectContainer):void{
			target.removeChildren(0,-1,false);
		}

		// Fonctions pour le life cycle du wrapper -------------------------------------------------- //
        // ------------------------------------------------------------------------------------------ //
		
		// Désactive temporairement la page
		public function pause():void {
			statusStack.push("pause");
			_controler.pause();
			_isPaused = true;
			this.touchable = false;
		}
		
		// Réactive la page
		public function resume():void {
			statusStack.push("resume");
			_controler.resume();
			_isPaused = false;
			this.touchable = true;
		}

		// permet d'initialiser/installer la page
		public function start():void{
			_view.setupListeners();
			
			statusStack.push("start");
			_model.addEventListener(MVCEvent.START_ENDED, onStartEnded);
			addChild(_view);
			_controler.start();
			_isStarted = true;
		}

		public function startEnded():void{
			Starling.current.nativeStage.addEventListener(StageOrientationEvent.ORIENTATION_CHANGE,   onOrientationChange);
			Starling.current.nativeStage.addEventListener(StageOrientationEvent.ORIENTATION_CHANGING, onOrientationChanging);

            Starling.current.stage.addEventListener(ResizeEvent.RESIZE, onResize);
            Starling.current.nativeStage.addEventListener(flash.events.Event.RESIZE, onStageResize);

            NativeApplication.nativeApplication.addEventListener(NativeWindowBoundsEvent.RESIZE, onResizeWindow);
            NativeApplication.nativeApplication.addEventListener(NativeWindowBoundsEvent.RESIZING, onResizingWindow);
        }

    protected function onStageResize(event:flash.events.Event):void {
//        var app:NativeApplication = NativeApplication.nativeApplication;
//        var rect:Rectangle = app.activeWindow.bounds;
//        trace("onStageResize ---------------------------------------------");
//        trace(app.activeWindow.bounds);
//        trace(event.target.stageWidth,event.target.stageHeight)
        updateDimensions(event.target.stageWidth,event.target.stageHeight); // Met à jour les info sur l'affichage
    }
    protected function onResize(event:ResizeEvent):void {
        if(_displayData.width  != event.width || _displayData.height != event.height){
            updateDimensions(event.width, event.height); // Met à jour les info sur l'affichage
        }
    }

        private function updateDimensions(width:int, height:int):void {
            var scale:Number = Starling.current.contentScaleFactor;
            var viewPort:Rectangle = new Rectangle(0, 0, width, height);

            Starling.current.viewPort = viewPort;
            stage.stageWidth  = viewPort.width  / scale;
            stage.stageHeight = viewPort.height / scale;

            _displayData.width  = width;
            _displayData.height = height;

            _controler.update(); // Notifie le reste de l'application que l'affichage changé
        }
		
		private function onOrientationChanging(event:StageOrientationEvent):void {
			if(currentOrientation != event.afterOrientation){
				_controler.orientationChanging();
			}
		}
		
		private function onOrientationChange(event:StageOrientationEvent):void {
			if(currentOrientation != event.afterOrientation){
				currentOrientation = event.afterOrientation;
                _controler.orientationChanged();
                _controler.update();
			}
		}
		
		private function onStartEnded(event:Event):void {
			startEnded();
			statusStack.push("onStartEnded");
			event.target.removeEventListener(MVCEvent.START_ENDED, onStartEnded);
			_isInitialized = true;

            _controler.update();
			resume();
		}
		
		// Arrête tout fonctionnement de la page
		public function stop():void{
			Starling.current.nativeStage.removeEventListener(StageOrientationEvent.ORIENTATION_CHANGE, onOrientationChange);
			Starling.current.nativeStage.removeEventListener(StageOrientationEvent.ORIENTATION_CHANGING, onOrientationChanging);
            Starling.current.stage.removeEventListener(ResizeEvent.RESIZE, onResize);
            Starling.current.nativeStage.removeEventListener(flash.events.Event.RESIZE, onStageResize);
            NativeApplication.nativeApplication.removeEventListener(NativeWindowBoundsEvent.RESIZE, onResizeWindow);
            NativeApplication.nativeApplication.removeEventListener(NativeWindowBoundsEvent.RESIZING, onResizingWindow);
			
			statusStack.push("stop");
			_controler.stop();
			_isStarted = false;
        }

        private function onResizingWindow(event:NativeWindowBoundsEvent):void {
            trace("onResizingWindow fo real");
        }

        private function onResizeWindow(event:NativeWindowBoundsEvent):void {
            trace("onResizeWindow fo real");
//            if(_displayData.width  != event.afterBounds.width || _displayData.height != event.afterBounds.height){
//                updateDimensions(event.afterBounds.width, event.afterBounds.height); // Met à jour les info sur l'affichage
//            }
        }
		
		// Supprime / vide la page
		public override function dispose():void {
			statusStack.push("dispose");
			super.dispose();
			
			_controler.dispose();
			
			if(_view){
				removeAllChild(_view);
				
				// à revoir pour s'assurer de ne pas faire de memory leak ------------------------------------------------- //
				if(_view.parent)
					_view.parent.removeChild(_view);
			}
			_model	   = null;
			_view  	   = null;
			_controler = null;
			
			System.gc();
		}
		// Supprime / vide la page
		public function shallowDispose():void {
			statusStack.push("shallowDispose");
			super.dispose();
			
			removeAllChild(_view);
			
			System.gc();
		}
		
		protected function screen_addedToStageHandler(event:Event):void{
			
			this.removeEventListener(Event.ADDED_TO_STAGE, screen_addedToStageHandler);
			this.addEventListener(Event.REMOVED_FROM_STAGE, screen_removedFromStageHandler);
			
			statusStack.push("screen_addedToStageHandler");
			start();
		}
		
		protected function screen_removedFromStageHandler(event:Event):void{
			statusStack.push("screen_removedFromStageHandler");
			stop();
			this.addEventListener(Event.ADDED_TO_STAGE, screen_addedToStageHandler);
			this.removeEventListener(Event.REMOVED_FROM_STAGE, screen_removedFromStageHandler);
			//super.screen_removedFromStageHandler(event);
		}
		/*
		override public function set height(value:Number):void{
			super.height = _view.height = value;
		}
		override public function set width(value:Number):void{
			super.width = _view.width = value;
		}
		*/
	}
}