package com.drawm.ui.interactive
{
import com.drawm.ui.*;
	import flash.geom.Point;
	import starling.core.Starling;
	import starling.display.Quad;
	
	/**
	 * ...
	 * @author Damon Perron-Laurin
	 */
	public class InteractiveQuad extends Quad implements IInteractiveUI 
	{
		private var _id:String;
		public function InteractiveQuad(id:String, width:Number, height:Number, color:uint = 16777215, premultipliedAlpha:Boolean = true) {
			if (id == null)
				_id = UI.createUUID();
			else
				_id = id;
				
			UI.addUI(this);
			
			super(width, height, color, premultipliedAlpha);
			
		}
		
		public function get mouseX():int {
			return Starling.current.nativeStage.mouseX - localToGlobal(new Point(0,0)).x;
		}
		public function get mouseY():int{
			return Starling.current.nativeStage.mouseY - localToGlobal(new Point(0,0)).y;
		}
		
		public function up():void {}
		
		public function out():void {}
		
		public function down():void {}
		
		public function get id():String {
			return _id;
		}
		
	}

}