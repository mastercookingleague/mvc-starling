package com.drawm.ui.interactive {
import com.drawm.ui.*;

    import flash.geom.Point;

    import starling.core.Starling;
	import starling.textures.Texture;
	
	/**
	 * ...
	 * @author Damon Perron-Laurin
	 */
	public class InteractiveImage extends UIImage implements IInteractiveUI
	{
		
		public function InteractiveImage(id:String, texture:Texture = null){
			super(id, texture);
		}
		
		// Called by Controler ---------------------------------------------------------//
        public function up():void{}
        public function out():void{}
        public function down():void { }

        public function get mouseX():int {
            return Starling.current.nativeStage.mouseX - localToGlobal(new Point(0,0)).x;
        }
        public function get mouseY():int{
            return Starling.current.nativeStage.mouseY - localToGlobal(new Point(0,0)).y;
        }
	}
}