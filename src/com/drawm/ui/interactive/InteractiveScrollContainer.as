package com.drawm.ui.interactive {
import com.drawm.ui.*;
	
	import flash.geom.Point;
	
	import feathers.controls.ScrollContainer;
	
	import starling.core.Starling;
	
	public class InteractiveScrollContainer extends ScrollContainer implements IInteractiveUI {
		
		private var _id:String;
		private const zeroPoint:Point = new Point(0,0);
		private var _mouseChildren:Boolean = true;
		
		public function InteractiveScrollContainer(id:String = null) {
			if (id == null)
				_id = UI.createUUID();
			else
				_id = id;
			
			UI.addUI(this);
			
			super();
		}
		
		public function get mouseX():int {
			return Starling.current.nativeStage.mouseX - localToGlobal(zeroPoint).x;
		}
		public function get mouseY():int{
			return Starling.current.nativeStage.mouseY - localToGlobal(zeroPoint).y;
		}
		
		public function up():void {}
		
		public function out():void {}
		
		public function down():void {}
		
		public function get id():String {
			return _id;
		}
	}
}