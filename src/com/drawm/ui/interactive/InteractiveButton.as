package com.drawm.ui.interactive
{
import com.drawm.ui.*;
	import flash.geom.Point;
	import feathers.controls.Button;
	import starling.core.Starling;
	
	public class InteractiveButton extends Button implements IInteractiveUI{
		
		private var _id:String;
		
		public function InteractiveButton(id:String) {
			if (id == null)
				_id = UI.createUUID();
			else
				_id = id;
			
			UI.addUI(this);
			
			super();
		}
		
		public function get mouseX():int {
			return Starling.current.nativeStage.mouseX - localToGlobal(new Point(0,0)).x;
		}
		public function get mouseY():int{
			return Starling.current.nativeStage.mouseY - localToGlobal(new Point(0,0)).y;
		}
		
		public function up():void {}
		
		public function out():void {}
		
		public function down():void {}
		
		public function get id():String {
			return _id;
		}
	}
}