package com.drawm.ui.interactive {
import com.drawm.ui.*;

	public class InteractiveSprite extends SuperSprite implements IInteractiveUI {
		
		private var _id:String;
		
		public function InteractiveSprite(id:String){
			_id = id;
			super();
		}
		
		override public function get mouseX():int {
			return super.mouseX;
		}
		override public function get mouseY():int{
			return super.mouseY;
		}
		
		public function up():void {}
		
		public function out():void {}
		
		public function down():void {}
		
		public function get id():String {
			return _id;
		}
	}
}