package com.drawm.ui.interactive
{
import com.drawm.ui.*;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import starling.events.Event;
	
	public class InteractiveUI extends UI implements IInteractiveUI
	{
		public function InteractiveUI(id:String = null){
			this.touchable = true;
			super(id);
		}
		
		override protected function onAddedToStage(event:Event):void{
			super.onAddedToStage(event);
		}
		
		override protected function onRemovedFromStage(event:Event):void{
			super.onRemovedFromStage(event);
		}
		
	// Drag ------------------------------------------------------------------------//
		private var _dragBound:Rectangle;
		private var _isDragging : Boolean = false;
		
		public function startDrag(dragBound:Rectangle = null):void{
			_thisPos  = new Point(0,0);
			_touchPos = new Point(0,0);
			
			if(dragBound)
				_dragBound = dragBound;
			
			if(!_isDragging){
				addEventListener(Event.ENTER_FRAME, onMoveSprite);
				_isDragging = true;
			}
		}
		public function stopDrag():void{
			if(_isDragging){
				removeEventListener(Event.ENTER_FRAME, onMoveSprite);
				_dragBound  = null;
				_isDragging = false;
			}
		}
		
		public function get isDragging():Boolean{
			return _isDragging;
		}
		private var _thisPos   : Point;
		private var _touchPos  : Point;
		private function onMoveSprite(event:Event):void{
			// TODO : AJOUTE LE ORIGIN LOCK yeahyeahyeahyeah
			// Réuttilise les mêmes points pour ne pas en recréer à chaque frame
			_thisPos.x = this.x;
			_thisPos.y = this.y;
			_thisPos = parent.localToGlobal(_thisPos);
			
			_touchPos.x = nativeStage.mouseX;
			_touchPos.y = nativeStage.mouseY;
			
			if(_dragBound){
				if(_touchPos.x < _dragBound.x+_dragBound.width  && _touchPos.x > _dragBound.x){
					_thisPos.x = _touchPos.x;
				}
				if(_touchPos.y < _dragBound.y+_dragBound.height && _touchPos.y > _dragBound.y){
					_thisPos.y = _touchPos.y;
				}
			}
			
			_thisPos = parent.globalToLocal(_thisPos);
			if(_thisPos.x != this.x)
				this.x = _thisPos.x;
			if(_thisPos.y != this.y)
				this.y = _thisPos.y;
			
			// reset les points
			_thisPos.x = _thisPos.y = _touchPos.x = _touchPos.y = 0;
		}
		
	// Called by Controler ---------------------------------------------------------//
		public function up():void{}
		public function out():void{}
		public function down():void{}
	}
}