package com.drawm.ui
{
	import flash.geom.Point;
	import flash.display.Stage;
	
	import starling.display.Stage;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	import starling.display.Stage;
	
	public class SuperSprite extends Sprite {
		
		private static var _nativeStage   : flash.display.Stage;
		private static var _starlingStage : starling.display.Stage;
		
		private var _mouseChildren:Boolean = true;
		
		public function SuperSprite(){
			super();
			
			if(_nativeStage   == null)
				_nativeStage = Starling.current.nativeStage;
			
			if(_starlingStage == null)
				_starlingStage = Starling.current.stage;
		}
		
		override public function get touchable():Boolean {
			return super.touchable;
		}

		override public function set touchable(value:Boolean):void{
			super.touchable = value;
		}

		public function get mouseChildren():Boolean{
			return _mouseChildren;
		}
		public function set mouseChildren(value:Boolean):void{
			_mouseChildren = value;
		}
		public override function hitTest(localPoint:Point, forTouch:Boolean=false):DisplayObject{
			var target:DisplayObject = super.hitTest(localPoint, forTouch);
			if (_mouseChildren) 
				return target;
			else 
				return target ? this : null;
		}
		public function get starlingStage():starling.display.Stage{
			return _starlingStage;
		}
		public function get nativeStage():flash.display.Stage{
			return _nativeStage;
		}
		protected static function get starlingStage():starling.display.Stage{
			return _starlingStage;
		}
		protected static function get nativeStage():flash.display.Stage{
			return _nativeStage;
		}

		public function get mouseX():int {
			return _nativeStage.mouseX - localToGlobal(new Point(0,0)).x;
		}
		public function get mouseY():int{
			return _nativeStage.mouseY - localToGlobal(new Point(0,0)).y;
		}
	}
}