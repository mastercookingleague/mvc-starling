package com.drawm.ui.text {
	
	import com.drawm.ui.interactive.IInteractiveUI;
	import com.drawm.ui.text.UIText;
	
	import flash.geom.Point;
	
	import starling.core.Starling;
	
	public class InteractiveText extends UIText implements IInteractiveUI{
		public function InteractiveText(id:String) {
			super(id);
		}
		
		public function get mouseX():int {
			return Starling.current.nativeStage.mouseX - localToGlobal(new Point(0,0)).x;
		}
		public function get mouseY():int{
			return Starling.current.nativeStage.mouseY - localToGlobal(new Point(0,0)).y;
		}
		
		public function up():void {}
		
		public function out():void {}
		
		public function down():void {}
		
	}
}