package com.drawm.ui.text
{
	import starling.text.TextField;
	import starling.utils.HAlign;

	public class TextFormat{
		
		public var color    : uint    = 0;
		public var fontSize : int     = 12;
		public var bold     : Boolean = false;
		public var border   : Boolean = false;
		public var autoScale: Boolean = false;
		public var fontName : String  = "Verdana";
		public var hAlign   : String  = HAlign.LEFT;
		
		public function TextFormat(){}
		
		public function applyToTextfield(textField:TextField):void{
			textField.color     = color;
			textField.fontSize  = fontSize;
			textField.bold      = bold;
			textField.border    = border;
			textField.autoScale = autoScale;
			textField.fontName  = fontName;
			textField.hAlign    = hAlign;
		}
	}
}