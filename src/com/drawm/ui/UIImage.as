package com.drawm.ui 
{
	import starling.display.Image;
	import starling.textures.Texture;
	
	/**
	 * ...
	 * @author Damon Perron-Laurin
	 */
	public class UIImage extends Image implements IUI
	{

        private var _id:String;

        public function UIImage(id:String, texture:Texture){
            super(texture);

            _id = id;
            UI.addUI(this);
        }

        public function get id():String{
            return _id;
        }
	}
}